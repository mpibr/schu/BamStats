#ifndef PARSERPARAMETERS_H
#define PARSERPARAMETERS_H

#include <iostream>

class ParserParameters
{
public:
    ParserParameters();
    ~ParserParameters() = default;
    
    bool parse(int argc, char const *argv[]);

    std::string fileBam;
    std::string fileBed;

private:
    bool m_error;

    void error(const std::string &errorMessage);
    bool parameterCheck(const char *parameter,
                        const char *reference,
                        size_t lengthParameter,
                        size_t lengthReference);

    void help();
};

#endif