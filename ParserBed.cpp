#include "ParserBed.h"

ParserBed::ParserBed() :
    m_fhBed(nullptr),
    m_fhTabix(nullptr),
    m_map()
{
    
}


ParserBed::~ParserBed()
{
    if (m_fhBed != nullptr)
    {
        if (bgzf_close(m_fhBed) != 0)
        {
            error("failed to close BED file");
        }
    }
    
    if (m_fhTabix != nullptr)
        regidx_destroy(m_fhTabix);
}

void ParserBed::open(const std::string &fileName)
{
    // open BED bgzip
    m_fhBed = bgzf_open(fileName.c_str(), "r");
    if (!m_fhBed)
    {
        error("failed to open BED file" + fileName);
        return;
    }

    // open BED tabix index
    m_fhTabix = regidx_init(fileName.c_str(), ParserBed::tabixParse, ParserBed::tabixFree, sizeof(char*), NULL);
    if (m_fhTabix == nullptr)
    {
        error("failed to open BED index" + fileName);
        return;
    }
}


void ParserBed::parse()
{
    kstring_t str = {0, 0, NULL};
    uint32_t bedLines = 0;
    m_map = std::map<std::string, BedLine>();

    while (bgzf_getline(m_fhBed, '\n', &str) > 0)
    {
        //std::cout << str.s << std::endl;
        auto ss = std::stringstream(str.s);
        auto line = BedLine();
        ss >> line;
        line.parseExons();
        m_map[line.name] = line;

        bedLines++;
    }
    free(ks_release(&str));

    std::cerr << "BedLines: " << bedLines << std::endl;
}



int ParserBed::tabixParse(const char *line, char **chr_beg, char **chr_end, reg_t *reg, void *payload, void *usr)
{
    // Use the standard parser for CHROM,FROM,TO
    int i, ret = regidx_parse_tab(line,chr_beg,chr_end,reg,NULL,NULL);
    if ( ret!=0 ) return ret;

    // Skip the fields that were parsed above
    char *ss = (char*) line;
    while ( *ss && isspace(*ss) ) ss++;
    for (i=0; i<3; i++)
    {
        while ( *ss && !isspace(*ss) ) ss++;
        if ( !*ss ) return -2;  // wrong number of fields
        while ( *ss && isspace(*ss) ) ss++;
    }
    if ( !*ss ) return -2;

    // Parse the payload
    char *se = ss;
    while ( *se && !isspace(*se) ) se++;
    char **dat = (char**) payload;
    *dat = (char*) malloc(se-ss+1);
    memcpy(*dat,ss,se-ss+1);
    (*dat)[se-ss] = 0;
    return 0;
}


void ParserBed::tabixFree(void *payload)
{
    char **dat = (char**)payload;
    free(*dat);
}

void ParserBed::query(char *queryChrom, uint32_t queryStart, uint32_t queryEnd) const
{
    regitr_t tabixIterator;
    if (regidx_overlap(m_fhTabix, queryChrom, queryStart, queryEnd, &tabixIterator))
    {
        char *hit = REGITR_PAYLOAD(tabixIterator, char*);
        std::cout << hit << std::endl;
    }
    else
    {
        std::cout << "nohit" << std::endl;
    }
}

void ParserBed::error(const std::string &errorMessage)
{
    std::cerr << "ParserBed::Error" << std::endl;
    std::cerr << '\t' << errorMessage << std::endl;
}