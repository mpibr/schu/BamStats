#ifndef PARSERBAM_H
#define PARSERBAM_H

#include <iostream>
#include <htslib/sam.h>
#include <htslib/kstring.h>
#include <htslib/kseq.h>

#include "ParserBed.h"

class ParserBam
{
public:
    ParserBam();
    ~ParserBam();

    void open(const std::string &fileBam);
    void parse(const ParserBed &bed);
    

private:
    samFile *m_fhBam;
    hts_idx_t *m_fhBai;
    bam_hdr_t *m_header;
    bam1_t *m_bam;

    void error(const std::string &errorMessage);

    static inline uint32_t bam_calend(const bam1_core_t *core, const uint32_t *cigar);

};


#endif /* PARSERBAM_H */