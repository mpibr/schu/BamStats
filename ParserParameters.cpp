#include "ParserParameters.h"

ParserParameters::ParserParameters() :
    m_error(false)
{

}


bool ParserParameters::parse(int argc, char const *argv[])
{
    // check for correct number of parameters

    // parameter check
    for (int p = 1; p < argc; p++)
    {
        // current parameter
        std::string parameter = argv[p];
        std::string value = "";
        
        if (parameterCheck(argv[p], "-b", parameter.length(), 2) || 
            parameterCheck(argv[p], "--bam", parameter.length(), 5))
        {
            if (++p < argc)
                value = argv[p];

            if (value.empty())
            {
                error("invalid -b|--bam parameter");
                break;
            }

            fileBam = value;
        }
        else if (parameterCheck(argv[p], "-a", parameter.length(), 2) || 
                    parameterCheck(argv[p], "--bed", parameter.length(), 5))
        {
            if (++p < argc)
                value = argv[p];

            if (value.empty())
            {
                error("invalid -a|--bed parameter");
                break;
            }

            fileBed = value;
        }
        else if (parameterCheck(argv[p], "-h", parameter.length(), 2) || 
                    parameterCheck(argv[p], "--help", parameter.length(), 6))
        {
            help();
            break;
        }
        else
        {
            error("unknown parameter " + parameter);
            break;
        }

    }

    return !m_error;
}


void ParserParameters::error(const std::string &errorMessage)
{
    m_error = true;
    std::cerr << "ParserParameters::Error" << std::endl;
    std::cerr << '\t' << errorMessage << std::endl;
}



bool ParserParameters::parameterCheck(const char *parameter, 
                                      const char *reference, 
                                      size_t lengthParameter,
                                      size_t lengthReference)
{
    return (strncmp(parameter, reference, lengthParameter) == 0) && 
        (lengthParameter == lengthReference);
}



void ParserParameters::help()
{
    std::cerr << "BamStats" << std::endl;
    std::cerr << '\t' << "-b|--bam binary alignment file BAM" << std::endl;
    std::cerr << '\t' << "-a|--bed annotation file BED12" << std::endl;
    std::cerr << '\t' << "-h|--help parameter description" << std::endl;
}