#include <iostream>

//#include "ParserBam.h"
//#include "ParserBed.h"
#include "ParserParameters.h"
#include "MetaGene.h"

#include <sstream>
#include <htslib/bgzf.h>
#include <htslib/kstring.h>
#include <map>
#include "BedLine.h"


int main(int argc, char const *argv[])
{

    auto param = ParserParameters();
    //auto bam = ParserBam();
    //auto bed = ParserBed();
    auto metagene = MetaGene();
    
    if (!param.parse(argc, argv))
        return 1;

    //bed.open(param.fileBed);
    //bed.parse();

    //bam.open(param.fileBam);
    //bam.parse(bed);

    metagene.open(param.fileBed, param.fileBam);
    metagene.pileup();
    
    return 0;
}


