#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;

MAIN:
{
    my @filesBam = @ARGV;

    my %data = ();
    my @samples = ();

    foreach my $file (@filesBam)
    {
        my $name = basename($file, ".bam");
        #$name =~ "s/\_vs\_genome\_umi//g";
        $name =~ s/_vs_genome_umi//g;
        print STDERR "processing ", $name, "\n";
        push(@samples, $name);
        my $lines = 0;
        open (my $fh, "/Users/tushevg/Desktop/BamStats/build/BamStats --bed /Users/tushevg/Desktop/RiboData/bed/ncbiRefSeq_rn6_Sep2018.bed.gz --bam $file |") or die $!;
        while (<$fh>)
        {
            chomp($_);
            my ($label, $cds, $array) = split("\t", $_, 3);
            $data{$label}{"span"} = $cds;
            $data{$label}{$name} = $array;
            $lines++;
            #last if($lines == 100);
        }
        close($fh);
    }

    print "sample","\t","cdsSpan","\t",join("\t",@samples),"\n";
    foreach my $key (sort keys %data)
    {
        my $test = 0;
        foreach my $sampleName (@samples)
        {
            $test++ if(exists($data{$key}{$sampleName}));
        }
        next if ($test != scalar(@samples));
        
        print $key,"\t",$data{$key}{"span"};
        foreach my $sampleName (@samples)
        {
            print "\t", $data{$key}{$sampleName};
        }
        print "\n";
    }

}


