#!/usr/bin/perl

use warnings;
use strict;
use Set::IntervalTree;
use Bio::Cigar;
use Time::HiRes qw(time);

sub parseAnnotationFile($$);
sub mergeIntervals($);
sub createGeneModels($$);
sub parseAlignmentFile($$$);
sub printResultTable($);

MAIN:
{
    my $file_annotation = shift;
    my $file_alignment = shift;
    my %data = ();
    my %db = ();
    my %result = ();
    
    parseAnnotationFile(\%data, $file_annotation);
    createGeneModels(\%db, \%data);
    parseAlignmentFile(\%result, \%db, $file_alignment);
    printResultTable(\%result);
}

sub printResultTable($)
{
    my $r_result = $_[0];
    my @columns = ("total", "fpUTR","CDS","tpUTR","intron","initiate","elongate","terminate");
    print "# gene","\t";
    print join("\t",@columns),"\n";
    foreach my $key (sort keys %{$r_result})
    {
        print $key;
        foreach(@columns)
        {
            my $value = exists($r_result->{$key}{$_}) ? $r_result->{$key}{$_} : 0;
            print "\t",$value;
        }
        print "\n";
    }
}


sub parseAlignmentFile($$$)
{
    my $r_result = $_[0];
    my $r_db = $_[1];
    my $file_alignment = $_[2];
    
    my $line = 0;
    open(my $fh, "samtools view $file_alignment|") or die $!;
    while(<$fh>)
    {
        chomp($_);
        my @bamLine = split("\t", $_, 12);
        my $key = $bamLine[2] ;
        my $cigar = Bio::Cigar->new($bamLine[5]);
        my $queryStart = $bamLine[3] - 1;
        my $queryEnd = $queryStart + $cigar->reference_length;
        my $queryLength = length($bamLine[9]);
        
        if(exists($r_db->{$key}))
        {
            my $tree = $r_db->{$key};
            my $values = $tree->fetch($queryStart, $queryEnd);
            if (scalar(@{$values}) > 0)
            {
                # get the first gene in tree (most trees should be represented by single gene)
                my $package = $values->[0];
                my $gene = $package->[0];
                $r_result->{$gene}{"total"}++;
                
                # check exon hit
                my $exonTree = $package->[2];
                #my $exonList = $exonTree->fetch($queryStart, $queryEnd);
                #my $exonUp = $exonTree->fetch_nearest_up($queryStart);
                #my $exonDown = $exonTree->fetch_nearest_down($queryStart);
                my $exonList = $exonTree->fetch($queryStart, $queryStart + 1);
                print "Fetch ",$exonList->[0],"\t",scalar(@{$package->[-1]}),"\n";
                
                
                my $exonIdx;
                if (defined($exonIdx))
                {
                    my $exons = $package->[-1];
                    
                    
                    print $gene," exons ",scalar(@{$exons}),"\t",$exonIdx,"\n";
                    print $queryStart," - ",$queryEnd,"\t",$exons->[-1][0]," - ",$exons->[-1][1],"\n";
                    
=head
                    my $offsetRead = $queryStart - $exon->[$exonIdx][0];
                    $offsetRead += $package->[-2][$exonIdx - 1] if($exonIdx > 0);
                    my $tag = ($package->[1] eq "+") ? "fpUTR" : "tpUTR";
                    $tag = "CDS" if($offsetRead > $package->[5]);
                    $tag = ($package->[1] eq "+") ? "tpUTR" : "fpUTR" if ($offsetRead > $package->[6]);
                    $r_result->{$gene}{$tag}++;
=cut
                }
                else
                {
                    $r_result->{$gene}{"intron"}++;
                }

            }
            else
            {
                $r_result->{"intergenic"}{"total"}++;
            }
        }
        else
        {
            $r_result->{"intergenic"}{"total"}++;
        }
        
        $line++;
        last if($line == 100);
    }
    close($fh);
}



sub createGeneModels($$)
{
    my $r_db = $_[0];
    my $r_data = $_[1];
    
    foreach my $key (sort keys %{$r_data})
    {
        my ($chrom, $gene) = split(";", $key, 2);
        my @cds = @{$r_data->{$key}{"cds"}};
        my @exons = @{$r_data->{$key}{"exons"}};
        my $m_cds = mergeIntervals(\@cds);
        my $m_exons = mergeIntervals(\@exons);
        
        my $exonTree = Set::IntervalTree->new;
        my $blocksCount = scalar(@{$m_exons});
        my @linearSize = (0) x $blocksCount;
        my $thickStart = $m_cds->[0][0];
        my $thickEnd = $m_cds->[0][1];
        my $offsetTickStart = 0;
        my $offsetTickEnd = 0;
        for (my $k = 0; $k < $blocksCount; $k++)
        {
            # exon properties
            my $exonStart = $m_exons->[$k][0];
            my $exonEnd = $m_exons->[$k][1];
            my $exonSpan = $exonEnd - $exonStart;
            
            # populate exon tree
            $exonTree->insert($k, $exonStart, $exonEnd);
            
            # calculate linear offset
            if ($k == 0)
            {
                $linearSize[$k] = $exonSpan;
            }
            else
            {
                $linearSize[$k] = $linearSize[$k - 1] + $exonSpan;
            }
            
            # update tickStart offset
            if (($exonStart <= $thickStart) && ($thickStart <= $exonEnd))
            {
                $offsetTickStart = $thickStart - $exonStart;
                $offsetTickStart += $linearSize[$k - 1] if($k > 0);
            }
            
            # update tickEnd offset
            if (($exonStart <= $thickEnd) && ($thickEnd <= $exonEnd))
            {
                $offsetTickEnd = $thickEnd - $exonStart;
                $offsetTickEnd += $linearSize[$k - 1] if($k > 0);
            }
        }
        
        # create information package
        my $package =   [$gene,
                        $r_data->{$key}{"strand"},
                        $exonTree,
                        $thickStart,
                        $thickEnd,
                        $offsetTickStart,
                        $offsetTickEnd,
                        \@linearSize,
                        \@exons];
        
        # assign to tree
        my $chromStart = $m_exons->[0][0];
        my $chromEnd = $m_exons->[-1][1];
        if (!exists($r_db->{$chrom}))
        {
            $r_db->{$chrom} = Set::IntervalTree->new;
        }
        $r_db->{$chrom}->insert($package, $chromStart, $chromEnd);
    }
    
}



sub parseAnnotationFile($$)
{
    my $r_data = $_[0];
    my $file_bed = $_[1];
    
    # open file handle
    my $fh;
    if ($file_bed =~ m/\.bed.gz$/)
    {
        open($fh, "gunzip -c $file_bed|") or die $!;
    }
    elsif ($file_bed =~ m/\.gz$/)
    {
        open($fh, "<", $file_bed) or die $!;
    }
    
    # parse each line
    while(<$fh>)
    {
        chomp($_);
        my @bedLine = split("\t", $_, 12);
        my ($name, $gene) = split(";", $bedLine[3], 2);
        my @blockSizes = split(",", $bedLine[10]);
        my @blockStarts = split(",", $bedLine[11]);
        my $key = $bedLine[0] . ";" . $gene;
        
        # add cds to hash
        my $cdsStart = $bedLine[6];
        my $cdsEnd = $bedLine[7];
        if ($cdsStart == $cdsEnd)
        {
            $cdsStart = $bedLine[1];
            $cdsEnd = $bedLine[2];
        }
        push(@{$r_data->{$key}{"cds"}}, [$cdsStart, $cdsEnd]);
        
        # add exons to hash
        for (my $k = 0; $k < $bedLine[9]; $k++)
        {
            my $exonStart = $bedLine[1] + $blockStarts[$k];
            my $exonEnd = $exonStart + $blockSizes[$k];
            push(@{$r_data->{$key}{"exons"}}, [$exonStart, $exonEnd]);
        }
        
        # add strand to hash
        $r_data->{$key}{"strand"} = $bedLine[5];
    }
    
    close($fh);
}


sub mergeIntervals($)
{
    my $r_array = $_[0];
    
    # sort array by start and end
    @{$r_array} = sort {$a->[0] <=> $b->[0] ||
        $a->[1] <=> $b->[1]} @{$r_array};
    
    my @merged = ();
    push(@merged, $r_array->[0]);
    
    for (my $k = 1; $k < scalar(@{$r_array}); $k++)
    {
        if ($merged[-1][1] < $r_array->[$k][0])
        {
            push(@merged, $r_array->[$k]);
        }
        elsif ($merged[-1][1] < $r_array->[$k][1])
        {
            $merged[-1][1] = $r_array->[$k][1];
        }
    }
    
    return \@merged;
}
