#!/usr/bin/perl

use warnings;
use strict;
use Set::IntervalTree;
use Bio::Cigar;
use Time::HiRes qw(time);

sub parseAnnotationFile($$);
sub parseAlignmentFile($$$);
sub printResultTable($);

MAIN:
{
    my $file_annotation = shift;
    my $file_alignment = shift;
    my %data = ();
    my %result = ();
    
    parseAnnotationFile(\%data, $file_annotation);
    
    parseAlignmentFile(\%result, \%data, $file_alignment);
    
    printResultTable(\%result);
    
}

sub printResultTable($)
{
    my $result_ref = $_[0];
    foreach my $key (keys %{$result_ref})
    {
        print $key,"\t",$result_ref->{$key},"\n";
    }
}


sub parseAlignmentFile($$$)
{
    my $result_ref = $_[0];
    my $data_ref = $_[1];
    my $file_alignment = $_[2];
    
    my $line = 0;
    open(my $fh, "samtools view $file_alignment|") or die $!;
    while(<$fh>)
    {
        chomp($_);
        my @bamLine = split("\t", $_, 12);
        
        my $strand = '+';
        $strand = '-' if ($bamLine[1] & 16);
        
        
        my $key = $bamLine[2];
        my $cigar = Bio::Cigar->new($bamLine[5]);
        my $qposStart = $bamLine[3] - 1;
        my $qposEnd = $qposStart + $cigar->reference_length;
        
        my $id = "intergenic";
        
        # check if key is present
        if (exists($data_ref->{$key}))
        {
            my $tree = $data_ref->{$key};
            my $values = $tree->fetch($qposStart, $qposEnd);
            
            $id = scalar(@{$values});
            $id = "intergenic" if($id == 0);
        }
        
        # assign result counter
        $result_ref->{$id}++;
        
        $line++;
        last if($line == 10000);
    }
    close($fh);
}


sub parseAnnotationFile($$)
{
    my $data_ref = $_[0];
    my $file_annotation = $_[1];
    
    open(my $fh, "gunzip -c $file_annotation|") or die $!;
    while(<$fh>)
    {
        chomp($_);
        my @bedLine = split("\t", $_, 12);
        my ($name, $gene) = split(";", $bedLine[3], 2);
        my @blockSizes = split(",", $bedLine[-2]);
        my @blockStarts = split(",", $bedLine[-1]);
        my $key = $bedLine[0];
        
        if (!exists($data_ref->{$key}))
        {
            $data_ref->{$key} = Set::IntervalTree->new;
        }
        
        push(@bedLine, $name, $gene);
        $data_ref->{$key}->insert(\@bedLine, $bedLine[1], $bedLine[2]);
    }
    close($fh);
}




