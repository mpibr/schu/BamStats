#!/usr/bin/perl

use warnings;
use strict;
use Set::IntervalTree;

sub slurpBed($$);
sub mergeIntervals($);
sub createGeneModels($$);

MAIN:
{
    my $file_bed = shift;
    my %data = ();
    my %db = ();
    
    slurpBed(\%data, $file_bed);
    createGeneModels(\%db, \%data);
    
}


sub createGeneModels($$)
{
    my $r_db = $_[0];
    my $r_data = $_[1];
    
    foreach my $key (sort keys %{$r_data})
    {
        my ($chrom, $gene) = split(";", $key, 2);
        my @cds = @{$r_data->{$key}{"cds"}};
        my @exons = @{$r_data->{$key}{"exons"}};
        my $m_cds = mergeIntervals(\@cds);
        my $m_exons = mergeIntervals(\@exons);
        
        my $exonTree = Set::IntervalTree->new;
        my $blocksCount = scalar(@{$m_exons});
        my @linearSize = (0) x $blocksCount;
        my $thickStart = $m_cds->[0][0];
        my $thickEnd = $m_cds->[0][1];
        my $offsetTickStart = 0;
        my $offsetTickEnd = 0;
        for (my $k = 0; $k < $blocksCount; $k++)
        {
            # exon properties
            my $exonStart = $m_exons->[$k][0];
            my $exonEnd = $m_exons->[$k][1];
            my $exonSpan = $exonEnd - $exonStart;
            
            # populate exon tree
            $exonTree->insert($k, $exonStart, $exonEnd);
            
            # calculate linear offset
            if ($k == 0)
            {
                $linearSize[$k] = $exonSpan;
            }
            else
            {
                $linearSize[$k] = $linearSize[$k - 1] + $exonSpan;
            }
            
            # update tickStart offset
            if (($exonStart <= $thickStart) && ($thickStart <= $exonEnd))
            {
                $offsetTickStart = $thickStart - $exonStart;
                $offsetTickStart += $linearSize[$k - 1] if($k > 0);
            }
            
            # update tickEnd offset
            if (($exonStart <= $thickEnd) && ($thickEnd <= $exonEnd))
            {
                $offsetTickEnd = $thickEnd - $exonStart;
                $offsetTickEnd += $linearSize[$k - 1] if($k > 0);
            }
        }
        
        # create information package
        my $package = [$gene,
                       $r_data->{$key}{"strand"},
                       $exonTree,
                       $thickStart,
                       $thickEnd,
                       $offsetTickStart,
                       $offsetTickEnd,
                       \@linearSize,
                       \@exons];
        
        # assign to tree
        my $chromStart = $m_exons->[0][0];
        my $chromEnd = $m_exons->[-1][1];
        if (!exists($r_db->{$chrom}))
        {
            $r_db->{$chrom} = Set::IntervalTree->new;
        }
        $r_db->{$chrom}->insert($package, $chromStart, $chromEnd);
    }
    
}



sub slurpBed($$)
{
    my $r_data = $_[0];
    my $file_bed = $_[1];
    
    # open file handle
    my $fh;
    if ($file_bed =~ m/\.bed.gz$/)
    {
        open($fh, "gunzip -c $file_bed|") or die $!;
    }
    elsif ($file_bed =~ m/\.gz$/)
    {
        open($fh, "<", $file_bed) or die $!;
    }
    
    # parse each line
    while(<$fh>)
    {
        chomp($_);
        my @bedLine = split("\t", $_, 12);
        my ($name, $gene) = split(";", $bedLine[3], 2);
        my @blockSizes = split(",", $bedLine[10]);
        my @blockStarts = split(",", $bedLine[11]);
        my $key = $bedLine[0] . ";" . $gene;
        
        # add cds to hash
        my $cdsStart = $bedLine[6];
        my $cdsEnd = $bedLine[7];
        if ($cdsStart == $cdsEnd)
        {
            $cdsStart = $bedLine[1];
            $cdsEnd = $bedLine[2];
        }
        push(@{$r_data->{$key}{"cds"}}, [$cdsStart, $cdsEnd]);
        
        # add exons to hash
        for (my $k = 0; $k < $bedLine[9]; $k++)
        {
            my $exonStart = $bedLine[1] + $blockStarts[$k];
            my $exonEnd = $exonStart + $blockSizes[$k];
            push(@{$r_data->{$key}{"exons"}}, [$exonStart, $exonEnd]);
        }
        
        # add strand to hash
        $r_data->{$key}{"strand"} = $bedLine[5];
    }
    
    close($fh);
}


sub mergeIntervals($)
{
    my $r_array = $_[0];
    
    # sort array by start and end
    @{$r_array} = sort {$a->[0] <=> $b->[0] ||
                        $a->[1] <=> $b->[1]} @{$r_array};
    
    my @merged = ();
    push(@merged, $r_array->[0]);
    
    for (my $k = 1; $k < scalar(@{$r_array}); $k++)
    {
        if ($merged[-1][1] < $r_array->[$k][0])
        {
            push(@merged, $r_array->[$k]);
        }
        elsif ($merged[-1][1] < $r_array->[$k][1])
        {
            $merged[-1][1] = $r_array->[$k][1];
        }
    }
    
    return \@merged;
}


