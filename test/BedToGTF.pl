#!/usr/bin/perl

use warnings;
use strict;
use Set::IntervalTree;


sub printGTF($$$$$);
sub parseFeatures($$$$$$$);


MAIN:
{
    my $file_bed = shift;
    my $outside = 50;
    my $inside = 30;
    
    open(my $fh, "gunzip -c $file_bed|") or die $!;
    while(<$fh>)
    {
        chomp($_);
        my %data = ();
        
        # split bed line
        my ($chrom,
            $chromStart,
            $chromEnd,
            $name,
            $score,
            $strand,
            $thickStart,
            $thickEnd,
            $itemrgb,
            $blocks,
            $listBlockSizes,
            $listBlockStarts) = split("\t", $_, 12);
        my ($tid, $gene) = split(";", $name, 2);
        my @blockSizes = split(",", $listBlockSizes);
        my @blockStarts = split(",", $listBlockStarts);
        
        # split exons
        my @exons = ();
        my @blocks = ();
        my $tempStart = 0;
        my $offsetThickStart = 0;
        my $offsetThickEnd = 0;
        my $span = 0;
        for (my $k = 0; $k < $blocks; $k++)
        {
            # prepare exon coordinates
            my $exonStart = $chromStart + $blockStarts[$k];
            my $exonEnd = $exonStart + $blockSizes[$k];
            push(@exons, [$exonStart, $exonEnd, "exon"]);
            
            # prepare linear block coordinates
            my $tempEnd = $tempStart + $blockSizes[$k];
            push(@blocks, [$tempStart, $tempEnd]);
            $tempStart = $tempEnd;
            
            # accumulate linear length
            $span += $blockSizes[$k];
            
            # detect CDS start
            if (($exonStart <= $thickStart) && ($thickStart <= $exonEnd))
            {
                $offsetThickStart = $thickStart - $exonStart;
                $offsetThickStart += $blocks[$k - 1][1] if($k > 0);
            }
            
            # detect CDS end
            if (($exonStart <= $thickEnd) && ($thickEnd <= $exonEnd))
            {
                $offsetThickEnd = $thickEnd - $exonStart;
                $offsetThickEnd += $blocks[$k - 1][1] if($k > 0);
            }
        }
        
        # prepare features
        my @features = ();
        if ($thickStart < $thickEnd)
        {
            my $tagLeft = ($strand eq "+") ? "fpUTR" : "tpUTR";
            my $tagRight = ($strand eq "+") ? "tpUTR" : "fpUTR";
            my $left = [0, $offsetThickStart, $tagLeft];
            my $center = [$offsetThickStart, $offsetThickEnd, "CDS"];
            my $right = [$offsetThickEnd, $span, $tagRight];
            my $fTree = Set::IntervalTree->new;
            $fTree->insert($left, $left->[0], $left->[1]) if($left->[0] < $left->[1]);
            $fTree->insert($center, $center->[0], $center->[1]) if($center->[0] < $center->[1]);
            $fTree->insert($right, $right->[0], $right->[1]) if($right->[0] < $right->[1]);
            
            # loop through exons
            for (my $k = 0; $k < $blocks; $k++)
            {
                my $qryStart = $blocks[$k][0];
                my $qryEnd = $blocks[$k][1];
                my $obj = $fTree->fetch($qryStart, $qryEnd);
                if (scalar(@{$obj}) == 1)
                {
                    push(@features, [$exons[$k][0], $exons[$k][1], $obj->[0][2]]);
                }
                else
                {
                    # sort by coordinate
                    @{$obj} = sort {$a->[0] <=> $b->[0]} @{$obj};
                    foreach (@{$obj})
                    {
                        my $refStart = $_->[0];
                        my $refEnd = $_->[1];
                        my $tag = $_->[2];
                        
                        my $featureStart;
                        if ($refStart <= $qryStart)
                        {
                            $featureStart = $exons[$k][0];
                        }
                        else
                        {
                            $featureStart = $exons[$k][0] + ($refStart - $qryStart);
                        }
                        
                        my $featureEnd;
                        if ($qryEnd <= $refEnd)
                        {
                            $featureEnd = $exons[$k][1];
                        }
                        else
                        {
                            $featureEnd = $exons[$k][0] + ($refEnd - $qryStart);
                        }
                        
                        push(@features, [$featureStart, $featureEnd, $_->[2]]);
                        
                    }
                    
                }
            }
        }
        
        # prepare regions
        my @regions = ();
        if (($offsetThickEnd - $offsetThickStart) > 2*$inside)
        {
            my $tagLeft = ($strand eq "+") ? "initiation" : "termination";
            my $tagRight = ($strand eq "+") ? "termination" : "initiation";
            my $leftStart = $offsetThickStart - $outside;
            $leftStart = 0 if($leftStart < 0);
            my $leftEnd = $offsetThickStart + $inside;
            my $rightStart = $offsetThickEnd - $inside;
            my $rightEnd = $offsetThickEnd + $outside;
            $rightEnd = $span if($rightEnd > $span);
            
            my $left = [$leftStart, $leftEnd, $tagLeft];
            my $center = [$leftEnd, $rightStart, "elongation"];
            my $right = [$rightStart, $rightEnd, $tagRight];
            my $fTree = Set::IntervalTree->new;
            $fTree->insert($left, $left->[0], $left->[1]) if($left->[0] < $left->[1]);
            $fTree->insert($center, $center->[0], $center->[1]) if($center->[0] < $center->[1]);
            $fTree->insert($right, $right->[0], $right->[1]) if($right->[0] < $right->[1]);
            
            # loop through exons
            for (my $k = 0; $k < $blocks; $k++)
            {
                my $qryStart = $blocks[$k][0];
                my $qryEnd = $blocks[$k][1];
                my $obj = $fTree->fetch($qryStart, $qryEnd);
                if (scalar(@{$obj}) == 1)
                {
                    push(@regions, [$exons[$k][0], $exons[$k][1], $obj->[0][2]]);
                }
                else
                {
                    # sort by coordinate
                    @{$obj} = sort {$a->[0] <=> $b->[0]} @{$obj};
                    foreach (@{$obj})
                    {
                        my $refStart = $_->[0];
                        my $refEnd = $_->[1];
                        my $tag = $_->[2];
                        
                        my $featureStart;
                        if ($refStart <= $qryStart)
                        {
                            $featureStart = $exons[$k][0];
                        }
                        else
                        {
                            $featureStart = $exons[$k][0] + ($refStart - $qryStart);
                        }
                        
                        my $featureEnd;
                        if ($qryEnd <= $refEnd)
                        {
                            $featureEnd = $exons[$k][1];
                        }
                        else
                        {
                            $featureEnd = $exons[$k][0] + ($refEnd - $qryStart);
                        }
                        
                        push(@regions, [$featureStart, $featureEnd, $_->[2]]);
                        
                    }
                    
                }
            }
        }
        
        
        
        
        
        #printGTF($chrom, $strand, $gene, $tid, \@exons);
        #printGTF($chrom, $strand, $gene, $tid, \@features) if(scalar(@features) > 0);
        printGTF($chrom, $strand, $gene, $tid, \@regions) if(scalar(@regions) > 0);
        
        
        
    }
    close($fh);

}


sub printGTF($$$$$)
{
    my $chrom = $_[0];
    my $strand = $_[1];
    my $geneId = $_[2];
    my $transcriptId = $_[3];
    my $exons = $_[4];
    
    foreach (@{$exons})
    {
        next if($_->[0] == $_->[1]);
        print $chrom,"\t";
        print "MPIBR","\t";
        print $_->[2],"\t";
        print $_->[0],"\t";
        print $_->[1],"\t";
        print ".","\t",$strand,"\t",".","\t";
        print "gene_id \"",$geneId,"\"; transcript_id \"",$transcriptId,"\";","\n";
    }
}
