#include "ParserBam.h"

ParserBam::ParserBam() :
    m_fhBam(nullptr),
    m_fhBai(nullptr),
    m_header(nullptr),
    m_bam(nullptr)
{

}

ParserBam::~ParserBam()
{
    if (m_bam != nullptr)
        bam_destroy1(m_bam);

    if (m_header != nullptr)
        bam_hdr_destroy(m_header);
    
    if (m_fhBai != nullptr)
        hts_idx_destroy(m_fhBai);

    if (m_fhBam != nullptr)
        sam_close(m_fhBam);
}

void ParserBam::open(const std::string &fileBam)
{
    // open bam file handle
    m_fhBam = hts_open(fileBam.c_str(), "r");
    if (m_fhBam == nullptr)
    {
        error("failed to open BAM file " + fileBam + " to read");
        return;
    }

    // read bam header
    m_header = sam_hdr_read(m_fhBam);
    if (m_header == nullptr)
    {
        error("failed to read BAM header");
        return;
    }

    // open bam index
    std::string fileBai = fileBam + ".bai";
    m_fhBai = sam_index_load(m_fhBam, fileBai.c_str());
    if (m_fhBai == nullptr)
    {
        error("failed to read BAM index" + fileBai);
        return;
    }

    // initialise bam record
    m_bam = bam_init1();
}


void ParserBam::parse(const ParserBed &bed)
{
    uint32_t lines = 0;
    while (sam_read1(m_fhBam, m_header, m_bam) >= 0)
    {
        lines++;

        char *chrom = m_header->target_name[m_bam->core.tid] ; //contig name (chromosome)
        uint32_t readStart = m_bam->core.pos;
        uint32_t readEnd = bam_calend(&m_bam->core, bam_get_cigar(m_bam));
        uint32_t readLength = m_bam->core.l_qseq; //length of the read

        std::cout << chrom << '\t' << readStart << '\t' << readEnd << '\t' << readLength << '\t';

        bed.query(chrom, readStart, readEnd);

    }
    std::cout << "BAM Lines: " << lines << std::endl;
}


void ParserBam::error(const std::string &errorMessage)
{
    std::cerr << "ParserBam::Error" << std::endl;
    std::cerr << '\t' << errorMessage << std::endl;
}

/* bam_calend
 * Calculate the rightmost coordinate of an alignment on the reference genome.
 * bam.h https://github.com/samtools/samtools/search?utf8=%E2%9C%93&q=bam_calend
 */

inline uint32_t ParserBam::bam_calend(const bam1_core_t *core, const uint32_t *cigar)
{
    return core->pos + (core->n_cigar ? bam_cigar2rlen(core->n_cigar, cigar) : 1);
}