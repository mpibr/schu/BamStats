#ifndef PARSERBED_H
#define PARSERBED_H

#include <iostream>
#include <sstream>
#include <map>

#include <htslib/bgzf.h>
#include <htslib/regidx.h>
#include <htslib/kstring.h>

#include "BedLine.h"

class ParserBed
{
public:
    ParserBed();
    ~ParserBed();

    void open(const std::string &fileName);
    void parse();
    void query(char *queryChrom, uint32_t queryStart, uint32_t queryEnd) const;

    std::map<std::string, BedLine> m_map;

private:
    BGZF *m_fhBed;
    regidx_t *m_fhTabix;
    static int tabixParse(const char *line, char **chr_beg, char **chr_end, reg_t *reg, void *payload, void *usr);
    static void tabixFree(void *payload);
    void error(const std::string &errorMessage);

};

#endif /* PARSERBED_H */